# Clojure README

## Server setup

### Java 8 on server

1. Download Java server JRE from Oracle at [www.oracle.com/technetwork/java/javase/downloads/server-jre8-downloads-2133154.html](http://www.oracle.com/technetwork/java/javase/downloads/server-jre8-downloads-2133154.html)
2. Update package manager: `apt-get update`
3. Install java package: `apt-get install java-package`
4. Create deb file as a non-root user: `fakeroot make-jpkg server-jre-8u111-linux-x64.tar.gz`
5. Install created deb file as root: `dpkg -i /home/whisper/oracle-java8-jre_8u111_amd64.deb`

### Setting daemon with systemd

Create a new unit /etc/systemd/system/webserver.service with this content:

```
[Unit]
Description=Clojure Webserver Daemon

[Service]
ExecStart=/usr/bin/java -jar /home/whisper/current/server.jar
User=whisper

[Install]
WantedBy=multi-user.target
```

Execute as root:

```
touch /etc/systemd/system/webserver.service
chmod 664 /etc/systemd/system/webserver.service
```

Add unit to systemd:

```
systemctl daemon-reload
systemctl start webserver.service
```

### Starting and stopping daemonized application

`systemctl start webserver.service` and `systemctl stop webserver.service`

### Setting up nginx reverse proxy

Create webserver file in /etc/nginx/sites-available and symlink it into sites-enabled with `ln -s /etc/nginx/sites-available/webserver /etc/nginx/sites-enabled/`

Example config:

```
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /home/whisper/current/public;

	server_name _;

	location ~ {
        	try_files $uri @proxy;
        }

	location @proxy {
                proxy_pass       http://localhost:8080;
                proxy_set_header Host      $host;
                proxy_set_header X-Real-IP $remote_addr;
	}
}
```

Important thing: named route (@proxy) must be last in try_files!